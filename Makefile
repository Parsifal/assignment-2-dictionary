.PHONY: clean all test
ASM=nasm
ASMFLAGS=-f elf64
LD=ld
PYTHON=python3
all: program clean
%.o: %.asm
	@$(ASM) $(ASMFLAGS) -o $@ $<
main.o: lib.inc words.inc dict.inc colon.inc
dict.o: lib.inc
program: main.o lib.o dict.o
	@$(LD) -o $@ $^
clean:
	@$(RM) *.o
test:
	$(PYTHON) test.py
