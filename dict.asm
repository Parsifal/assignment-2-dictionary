%include "lib.inc"
global find_word
section .text
find_word:
    mov r14, rdi
    mov r12, rdx
.loop:
    mov rdi, r14
    mov rsi, r12

    mov r13, rsi
    add rsi, 8
    call string_equals
    test rax, rax
    jz .end
    mov rax, r13
    add rax, 8

    mov rdi, rax
    test rdi, rdi
    jz .end
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    call print_string
    mov rax, 1
    ret
.end:
    mov r12, [r12]
    test r12, r12
    jnz .loop
    xor rax, rax
    ret