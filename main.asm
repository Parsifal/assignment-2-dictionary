%define BUFFER_SIZE 256

%macro exit 0
    mov rdi, 1
    call exit
%endmacro

%macro error 1
    mov rdi, 2                  ; Установка значения 2 в регистр RDI для файлового дескриптора stderr
    mov rsi, %1                 ; Установка адреса строки сообщения об ошибке (первый аргумент макроса) в RSI
    mov rdx, %1_end - %1 - 1    ; Вычисление длины строки (без нуль-терминатора) и запись в RDX
    mov rax, 1                  ; Установка системного вызова write в значение RAX (1 в x86-64 Linux)
    syscall
%endmacro

global _start
%include "colon.inc"
%include "lib.inc"
%include "dict.inc"
%include "words.inc"


section .rodata
    key_not_found_err: db "Key not found", 0
    key_not_found_err_end:
    key_is_too_long_err: db "Key is too long", 0
    key_is_too_long_err_end:
section .bss
    word_buf: resb BUFFER_SIZE
section .text
_start:
    section .text
    mov rdi, word_buf
    mov rsi, BUFFER_SIZE
    call read_word
    mov rdi, word_buf
    test rax, rax
    jz .key_is_too_long_err
    mov rdx, last
    call find_word
    test rax, rax
    jz .key_not_found_err
    exit
.key_is_too_long_err:
    error key_is_too_long_err
    exit
.key_not_found_err:
    error key_not_found_err
    exit
