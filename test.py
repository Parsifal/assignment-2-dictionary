import subprocess

OK = '\033[92m'
FAIL = '\033[91m'
END = '\033[0m'

stdin = ['', 'word', 'word1', 'word2', 'word3', 'w' * 300]
stdout = ['', '', 'word1 explanation', 'word2 explanation', 'word3 explanation', '']
stderr = ['Key not found', 'Key not found', '', '', '', 'Key is too long']
for ind, (s_in, s_out, s_err) in enumerate(zip(stdin, stdout, stderr)):
    subprocess.call(['make'])
    process = subprocess.Popen(['./program'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, error = [i.decode() for i in process.communicate(input=s_in.encode())]
    if output == s_out and error == s_err:
        print(f'{OK}Test {ind + 1} passed{END}')
    else:
        print(f'{FAIL}Test {ind + 1} failed{END}')
    print(f'stdout: {output}')
    print(f'stderr: {error}')
